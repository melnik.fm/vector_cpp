﻿#include <iostream>
#include <math.h>

class Vector
{
private:
	double x,y,z;
public:
	Vector() : x(0), y(0), z(0)
	{}

	Vector( double _x, double _y, double _z )
	{
		x = _x;
		y = _y;
		z = _z;
    }

	double LengthVector()
	{
		return(sqrt(pow(x,2) + pow(y,2) + pow(z,2)));
	}

	void Show()
	{
		std::cout << "Vector: x = " << x << ", y = " << y << ", z = " << z <<  std::endl;
		
	}

};

int main()
{
	Vector First (3, 4, 5);
	First.Show();
	std::cout << "Length = " << First.LengthVector() << std::endl;
	return 0;
}

